Red [
    Needs: 'View
    Author: "Haze System"
]

by: make op! :as-pair

draw-freq: 5
window-size: 480x320
grid-size: 16
snake: reactor [
    tl: ((window-size/x / 2) - grid-size) by ((window-size/y / 2) - grid-size)
    br: is [ (tl/x + grid-size) by (tl/y + grid-size) ]
    direc: "right"
    speed: 1
]

in-canvas: func [] [
    canvas/rate: draw-freq
    move-snake snake

    canvas/draw: reduce [
        'line-width 0
        'fill-pen crimson 
        'box snake/tl snake/br
    ]
]

key-down: func [ event ] [
    switch event/key [
        up    [ unless snake/direc == "down"  [ snake/direc: "up" ] ]
        down  [ unless snake/direc == "up"    [ snake/direc: "down" ] ]
        left  [ unless snake/direc == "right" [ snake/direc: "left" ] ]
        right [ unless snake/direc == "left"  [ snake/direc: "right" ] ]
    ]
]

move-snake: func [ snake ] [
    switch snake/direc [
        "up"    [ either snake/tl/y == negate grid-size
                    [ snake/tl/y: window-size/y ]
                    [ snake/tl/y: snake/tl/y - grid-size ] ]
        "down"  [ either snake/tl/y == window-size/y
                    [ snake/tl/y: negate grid-size ]
                    [ snake/tl/y: snake/tl/y + grid-size ] ]
        "left"  [ either snake/tl/x == negate grid-size
                    [ snake/tl/x: window-size/x ]
                    [ snake/tl/x: snake/tl/x - grid-size ] ]
        "right" [ either snake/tl/x == window-size/x
                    [ snake/tl/x: negate grid-size ]
                    [ snake/tl/x: snake/tl/x + grid-size ] ]
    ]
]

view [
    title "Snake"
    canvas: base focus window-size black
    on-time [ in-canvas ]
    on-key-down [ key-down event ]
    
    do [
        canvas/rate: draw-freq
        canvas/draw: copy []
    ]
]